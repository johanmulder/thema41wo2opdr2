// Global var containing the memory table.
memoryTable = [];
// Default timeout after which an item is hidden when 2 items have been selected.
itemHideTimeout = 1000;
// This array will contain the selected items.
selectedItems = null;
// This array will contain the matched items.
matchedItems = [];
// This var contains the start time of the game.
timeStart = null;

/**
 * Randomize a given string.
 * @param str The string to randomize.
 * @returns {string} The randomized string
 */
function randomizeString(str) {
    var randomLetters = [];
    for (var i = 0; i < str.length; i++) {
        // Get a free letter.
        var letter = '';
        do
        {
            letter = str[Math.floor(Math.random() * str.length)];
        } while (randomLetters.indexOf(letter) !== -1);
        randomLetters.push(letter);
    }

    return randomLetters.join('');
}

/**
 * Check if array 'a' is filled with 'maxEntries' entries.
 * @param a
 * @param maxEntries
 * @returns {boolean}
 */
function isArrayFilled(a, maxEntries) {
    for (var i = 0; i < maxEntries; i++) {
        if (a[i] == undefined)
            return false;
    }
    return true;
}

/**
 * Find a free position in an array. Only call this when you're
 * certain the array hasn't been filled completely.
 * @param a The array.
 * @param maxEntries Maximum amount of entries
 * @returns int
 */
function findFreePosition(a, maxEntries) {
    var pos;
    do
    {
        pos = Math.floor(Math.random() * maxEntries);
    } while (pos in a);
    return pos;
}

/**
 * Build the memory table.
 * @param maxEntries Maximum amount of entries in the table.
 */
function buildMemoryTable(maxEntries) {
    var memoryTable = [];
    // Generate a random string.
    var randomString = randomizeString('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    for (var i = 0; i < Math.ceil(maxEntries / 2); i++) {
        var letter = randomString[i];
        memoryTable[findFreePosition(memoryTable, maxEntries)] = letter;
        if (!isArrayFilled(memoryTable, maxEntries))
            memoryTable[findFreePosition(memoryTable, maxEntries)] = letter;
    }
    return memoryTable;
}

function setItemValue(id, value) {
    // Set the string value.
    $('#item' + id).html(value);
}

/**
 * Function which is called when an item has been clicked.
 * @param item
 */
function itemClicked(item) {
    // Get the id and memory table position.
    var id = item.attr('id');
    var clickedPos = parseInt(id.replace('item', ''));

    // If these conditions match, do nothing.
    // 1. If the clicked item has already been matched.
    // 2. If 2 items have been selected.
    if (matchedItems.indexOf(clickedPos) !== -1 ||
        selectedItems != null && selectedItems.length == 2) {
        return;
    }

    // Check what to do.
    if (selectedItems === null) {
        // No item has been selected. Open up the selected item.
        selectedItems = [];
        selectedItems.push(clickedPos);
        setItemValue(clickedPos, memoryTable[clickedPos]);
        // Check if an item was clicked before.
        if (timeStart === null)
            // Get the current time.
            timeStart = getCurrentTime();
    }
    else if (selectedItems[0] != clickedPos) {
        // The clicked item was not the same. compare the values.
        setItemValue(clickedPos, memoryTable[clickedPos]);
        selectedItems.push(clickedPos);
        if (memoryTable[selectedItems[0]] == memoryTable[clickedPos]) {
            // Mark both items as matched.
            matchedItems.push(selectedItems[0]);
            matchedItems.push(clickedPos);
            selectedItems = null;

            if (memoryTable.length - matchedItems.length <= 1) {
                alert('Gefeliciteerd! Je hebt alles gematcht! Totale speeltijd: ' +
                    (getCurrentTime() - timeStart) + ' seconden');
            }
        }
        else {
            // Call the anonymous function after "itemHideTimeout" milliseconds.
            setTimeout(function() {
                // Reset the values on both selected items.
                setItemValue(selectedItems[0], '*');
                setItemValue(selectedItems[1], '*');
                selectedItems = null;
            }, itemHideTimeout);
        }
    }
}

/**
 * Create a memory div item
 * @param index
 * @param value
 * @returns {*|jQuery|HTMLElement}
 */
function createMemoryItem(index, value) {
    // Create a new div element, jquery style.
    var elem = $(document.createElement('div'));
    elem.addClass('memory_item');
    elem.html(value);
    elem.attr('id', 'item' + index);
    elem.on('click', function() {
        itemClicked($(this));
    });
    return elem;
}

/**
 * Get the current time in epoch seconds.
 * @returns {number}
 */
function getCurrentTime() {
    // Divide by 1000, because getTime returns milliseconds as well.
    return Math.floor(new Date().getTime() / 1000);
}

/**
 * Set the global item hide timeout.
 * @param newVal
 */
function setItemHideTimeout(newVal) {
    itemHideTimeout = newVal;
    $('#currentItemHideTimeout').html(newVal);
    $('#itemHideTimeout').val(newVal);
}

/**
 * Function which is called when the document loads.
 */
$(document).ready(function() {
    // Build the memory table.
    memoryTable = buildMemoryTable(25);
    for (var i = 0; i < memoryTable.length; i++) {
        $('#memoryfield').append(createMemoryItem(i, '*'));
    }
    console.log(memoryTable);

    // Set default item hide timeout values.
    setItemHideTimeout(itemHideTimeout);
    // When the slider slides, change the global hide timeout.
    $('#itemHideTimeout').change(function() {
        setItemHideTimeout($('#itemHideTimeout').val());
    });
});
